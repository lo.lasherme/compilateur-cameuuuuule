open Arbre

let _ =
   try
      let lexbuf = Lexing.from_channel stdin in
         let ast = Parseur.prog Lexeur.token lexbuf in
            (*print_string (affiche ast ^ "\n");*)
            check_ast ast; print_string "\n";

   with
   | Lexeur.TokenInconnu -> Printf.printf ("\nErreur Token\n")
   | Parsing.Parse_error -> Printf.printf ("\nErreur\n")
