{
open Parseur
open Lexing

exception TokenInconnu
}
rule token = parse
         |['\n' '\r' '\t' ' '] {token lexbuf}

         |"if" {IF}
         |"then" {THEN}
         |"else" {ELSE}

         |"true" {TRUE}
         |"false" {FALSE}
         |"()" {UNIT}
         |"error" {ERROR}

         |"Unit" {TUNIT}
         |"Bool" {TBOOL}
         |"Int" {TINT}

         |"ref" {REF}
         |"let" {LET}
         |"rec" {REC}
         |"in" {IN}
         |"fun" {FUN}
         |"fix" {FIX}
         |"try" {TRY}
         |"with" {WITH}
         |"or" {OR}
         |"and" {AND}

         |'+' {PLUS}
         |'-' {MOINS}
         |'*' {MULT}
         |'/' {DIV}

         |'(' {PARG}
         |')' {PARD}
         |';' {PTVIRG}
         |',' {VIRG}

         |'=' {EGL}
         |'>' {SUP}
         |'<' {INF}
         |">=" {SUPEGL}
         |"<=" {INFEGL}
         |"!=" {DIF}

         |"!" {DEREF}
         |":=" {AFCT}
         |"->" {FLECHE}

         |eof {EOF}

         |['1'-'9']*['0'-'9']+ as n {INT (int_of_string n)}
         |['a'-'z'](['a'-'z']|['A'-'Z']|['0'-'9']|'_')* as s {IDF s}

         |_ {raise TokenInconnu}
