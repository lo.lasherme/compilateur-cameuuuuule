type expression_ast =
   | Unit
   | Int of int
   | Bool of bool
   | Plus of expression_ast * expression_ast
   | Moins of expression_ast * expression_ast
   | Mult of expression_ast * expression_ast
   | Div of expression_ast * expression_ast
   | Egl of expression_ast * expression_ast
   | Sup of expression_ast * expression_ast
   | Inf of expression_ast * expression_ast
   | SupEgl of expression_ast * expression_ast
   | InfEgl of expression_ast * expression_ast
   | Dif of expression_ast * expression_ast
   | Var of string
   | Deref of expression_ast
   | Ref of expression_ast
   | Afct of expression_ast * expression_ast
   | Seq of expression_ast * expression_ast
   | Cdt of expression_ast * expression_ast * expression_ast
   | Let of string * expression_ast list * expression_ast * expression_ast
   | Rec of string * expression_ast list * expression_ast * expression_ast
   | Apl of expression_ast * expression_ast
   | Pair of expression_ast * expression_ast
   | Par of expression_ast
   | Fun of string * expression_ast
;;

let string_bool b =
   match b with
      | true -> "true"
      | false -> "false"
;;

let rec string_argument a =
   match a with
      | [] -> " "
      | hd::tl -> match hd with
                     | Unit ->  " ()" ^ string_argument tl
                     | Var s -> " " ^ s ^ string_argument tl
                     | _ -> failwith "Problème string_argument"
;;

let rec affiche = function
   | Unit -> "()"
   | Int(n) -> string_of_int n
   | Bool(b) -> string_bool b
   | Plus(e1, e2) -> affiche e1 ^ " + " ^ affiche e2
   | Moins(e1, e2) -> affiche e1 ^ " - " ^ affiche e2
   | Mult(e1, e2) -> affiche e1 ^ " * " ^ affiche e2
   | Div(e1, e2) -> affiche e1 ^ " / " ^ affiche e2
   | Egl(e1, e2) -> affiche e1 ^ " = " ^ affiche e2
   | Sup(e1, e2) -> affiche e1 ^ " > " ^ affiche e2
   | Inf(e1, e2) -> affiche e1 ^ " < " ^ affiche e2
   | SupEgl(e1, e2) -> affiche e1 ^ " >= " ^ affiche e2
   | InfEgl(e1, e2) -> affiche e1 ^ " <= " ^ affiche e2
   | Dif(e1, e2) -> affiche e1 ^ " != " ^ affiche e2
   | Var(s) -> s
   | Deref(e1) -> "!" ^ affiche e1;
   | Ref(e1) -> "ref " ^ affiche e1
   | Afct(e1, e2) -> affiche e1 ^ " := " ^ affiche e2
   | Seq(e1, e2) -> affiche e1 ^ ";\n" ^ affiche e2
   | Cdt(e1, e2, e3) -> "if " ^ affiche e1 ^ " then " ^ affiche e2 ^ " else " ^ affiche e3
   | Let(s, a, e1, e2) -> "let " ^ s ^ string_argument a ^ "= " ^ affiche e1 ^ " in\n" ^ affiche e2
   | Rec(s, a, e1, e2) -> "let rec " ^ s ^ string_argument a ^ "= " ^ affiche e1 ^ " in\n" ^ affiche e2
   | Pair(e1, e2) -> "(" ^ affiche e1 ^ ", " ^ affiche e2 ^ ")"
   | Apl(e1, e2) -> affiche e1 ^ " " ^ affiche e2
   | Par(e1) -> "(" ^ affiche e1 ^ ")"
   | Fun(s, e1) -> "fun " ^ s ^ " -> " ^ affiche e1
;;

let rec affiche_liste_ast l =
   match l with
      | [] -> "\n"
      | hd::tl -> affiche hd ^ "\n-----\n" ^ affiche_liste_ast tl
;;

let rec check_appel ld nom_apl =
   match ld with
      | [] -> false
      | hd::tl -> match hd with
                     | Let(s, a, e1, e2) -> if s = nom_apl then true else check_appel tl nom_apl
                     | Rec(s, a, e1, e2) -> if s = nom_apl then true else check_appel tl nom_apl
                     | Fun(s, e1) -> if s = nom_apl then true else check_appel tl nom_apl
                     | _ -> failwith "Problème check_appel"
;;

let rec check_appel_arbre ld nom_apl =
   match ld with
      | [] -> []
      | hd::tl -> match hd with
                     | Let(s, a, e1, e2) as n -> if s = nom_apl then n::[] else check_appel_arbre tl nom_apl
                     | Rec(s, a, e1, e2) as n-> if s = nom_apl then n::[] else check_appel_arbre tl nom_apl
                     | Fun(s, e1) as n -> if s = nom_apl then n::[] else check_appel_arbre tl nom_apl
                     | _ -> failwith "Problème check_appel"
;;

let rec check_argument ld args =
   match args with
      | Apl(e1, e2) ->  check_argument ld e1 @ check_argument ld e2
      | Var(s) -> if check_appel ld s then s::[] else []
      | _ -> []
;;

let rec check_argument_bis ld args =
   match args with
      | Apl(e1, e2) ->  check_argument_bis ld e1 @ check_argument_bis ld e2
      | Var(s) -> if check_appel ld s then check_appel_arbre ld s else []
      | _ -> []
;;

let rec string_liste l =
   match l with
   | [] -> ""
   | hd::tl -> hd ^ " " ^ string_liste tl
;;

let rec existe l el =
   match l with
      | [] -> false
      | hd::tl -> if el == hd then true else existe tl el
;;

let rec uniq_bis src dest =
   match src with
      | [] -> dest
      | hd::tl -> uniq_bis tl (if existe dest hd then dest else hd::dest)
;;

let uniq src =
   uniq_bis src []
;;

let rec est_fonction_bis = function
   | Unit -> 0
   | Int(n) -> 0
   | Bool(b) -> 0
   | Plus(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | Moins(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | Mult(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | Div(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | Egl(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | Sup(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | Inf(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | SupEgl(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | InfEgl(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | Var(s) -> 0
   | Dif(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | Deref(e1) -> est_fonction_bis e1
   | Ref(e1) -> est_fonction_bis e1
   | Afct(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | Seq(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | Cdt(e1, e2, e3) -> est_fonction_bis e1 + est_fonction_bis e2 + est_fonction_bis e3
   | Let(s, a, e1, e2) -> if List.length a > 0 then 1 else est_fonction_bis e1 + est_fonction_bis e2
   | Rec(s, a, e1, e2) -> if List.length a > 0 then 1 else est_fonction_bis e1 + est_fonction_bis e2
   | Pair(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | Apl(e1, e2) -> est_fonction_bis e1 + est_fonction_bis e2
   | Par(e1) -> est_fonction_bis e1
   | Fun(s, e1) -> 1
;;

let est_fonction = function
   | Let(s, a, e1, e2) -> if List.length a > 0 then true else est_fonction_bis e1 > 0
   | Rec(s, a, e1, e2) -> if List.length a > 0 then true else est_fonction_bis e1 > 0
   | Fun(s, e1) -> true
   | _ -> false
;;

let rec check_ast_bis ld b1 b2 lt = function
   | Unit -> ()
   | Int(n) -> ()
   | Bool(b) -> ()
   | Plus(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | Moins(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | Mult(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | Div(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | Egl(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | Sup(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | Inf(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | SupEgl(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | InfEgl(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | Dif(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | Var(s) -> ()
   | Deref(e1) -> check_ast_bis ld b1 b2 lt e1
   | Ref(e1) -> check_ast_bis ld b1 b2 lt e1
   | Afct(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | Seq(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | Cdt(e1, e2, e3) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2; check_ast_bis ld b1 b2 lt e3
   | Let(s, a, e1, e2) as n -> check_ast_bis ld b1 b2 lt e1; check_ast_bis (n::ld) b1 b2 lt e2
   | Rec(s, a, e1, e2) as n -> check_ast_bis (n::ld) b1 b2 lt e1; check_ast_bis (n::ld) b1 b2 lt e2
   | Pair(e1, e2) ->  check_ast_bis ld b1 b2 lt e1; check_ast_bis ld b1 b2 lt e2
   | Apl(e1, e2) as n ->   if b1
                           then
                              if b2
                              (*then print_string ((affiche n) ^ "\n")*)
                              then lt := (check_argument_bis ld n) @ !lt
                              else
                                 if not (check_appel ld (match e1 with | Var(s) -> s | _ -> failwith "Problème check_ast_bis"))
                                    (*then print_string((affiche n) ^ "\n");*)
                                    then lt := (check_argument_bis ld e2) @ n::[] @ !lt;


                           check_ast_bis ld false true lt e1;
                           check_ast_bis ld false true lt e2
   | Par(e1) ->  check_ast_bis ld true b2 lt e1
   | Fun(s, e1) as n -> check_ast_bis (n::ld) b1 b2 lt e1
;;

let nb_id = ref 0;;

let gen_id =
   nb_id := !nb_id + 1;
   "id_" ^ string_of_int !nb_id
;;

let push id =
   Apl (Var "push", Var id)
;;

let pop =
   Apl (Var "pop", Unit)
;;

let rec transformation lt = function
   | Unit -> Unit
   | Int(n) -> Int(n)
   | Bool(b) -> Bool(b)
   | Plus(e1, e2) -> Plus(transformation lt e1, transformation lt e2)
   | Moins(e1, e2) -> Moins(transformation lt e1, transformation lt e2)
   | Mult(e1, e2) -> Mult(transformation lt e1, transformation lt e2)
   | Div(e1, e2) -> Div(transformation lt e1, transformation lt e2)
   | Egl(e1, e2) -> Egl(transformation lt e1, transformation lt e2)
   | Sup(e1, e2) -> Sup(transformation lt e1, transformation lt e2)
   | Inf(e1, e2) -> Inf(transformation lt e1, transformation lt e2)
   | SupEgl(e1, e2) -> SupEgl(transformation lt e1, transformation lt e2)
   | InfEgl(e1, e2) -> InfEgl(transformation lt e1, transformation lt e2)
   | Dif(e1, e2) ->  Dif(transformation lt e1, transformation lt e2)
   | Var(s) -> Var(s)
   | Deref(e1) -> Deref(transformation lt e1)
   | Ref(e1) -> Ref(transformation lt e1)
   | Afct(e1, e2) -> Afct(transformation lt e1, transformation lt e2)
   | Seq(e1, e2) -> Seq(transformation lt e1, transformation lt e2)
   | Cdt(e1, e2, e3) -> Cdt(transformation lt e1, transformation lt e2, transformation lt e3)
   | Let(s, a, e1, e2) as n ->   if existe lt n && est_fonction n
                                 then  (let id_courant = gen_id in
                                          Let(s, a, Seq(push id_courant, Seq ((transformation lt e1), pop)), transformation lt e2))
                                 else Let(s, a, transformation lt e1, transformation lt e2)
   | Rec(s, a, e1, e2) as n ->   if existe lt n && est_fonction n
                                 then  (let id_courant = gen_id in
                                          Rec(s, a, Seq(push  id_courant, Seq ((transformation lt e1), pop)), transformation lt e2))
                                 else Rec(s, a, transformation lt e1, transformation lt e2)
   | Pair(e1, e2) ->  Pair(transformation lt e1, transformation lt e2)
   | Apl(e1, e2) -> Apl(transformation lt e1, transformation lt e2)
   | Par(e1) -> Par(transformation lt e1)
   | Fun(s, e1) -> Fun(s, transformation lt e1)
;;

let check_ast f =
   let liste_transfo = ref [] in
      check_ast_bis [] true false liste_transfo f;
      liste_transfo := uniq !liste_transfo;
      let new_ast = transformation !liste_transfo f in
      print_string (affiche new_ast)
;;











(* *)
