%{
   open Arbre

   exception Eof
%}

%token PLUS MOINS MULT DIV
%token PARG PARD PTVIRG VIRG
%token EGL SUP INF SUPEGL INFEGL DIF
%token DEREF AFCT FLECHE
%token IF THEN ELSE
%token TRUE FALSE UNIT ERROR
%token TUNIT TBOOL TINT
%token REF LET REC IN FUN FIX TRY WITH OR AND
%token <string> IDF
%token <int> INT
%token EOF

%left ELSE IN FLECHE
%left PTVIRG
%left AFCT
%nonassoc EGL DIF SUP SUPEGL INF INFEGL
%left PLUS MOINS
%left MULT DIV
%nonassoc REF

%type <Arbre.expression_ast> prog
%start prog
%%
prog:
     expression EOF {$1}
;

expression:
     appel_expression {$1}
   | expression PTVIRG expression {Seq ($1, $3)}
   | LET IDF liste_identifiants EGL expression IN expression {Let ($2, $3, $5, $7)}
   | LET REC IDF liste_identifiants EGL expression IN expression {Rec ($3, $4, $6, $8)}
   | FUN identifiant FLECHE expression {$4}
   | IF expression THEN expression ELSE expression {Cdt ($2, $4, $6)}
   | REF expression {Ref $2}
   | expression AFCT expression {Afct ($1, $3)}
   | operation {$1}
   | comparaison {$1}
;

operation:
     expression PLUS expression {Plus ($1, $3)}
   | expression MOINS expression {Moins ($1, $3)}
   | expression MULT expression {Mult ($1, $3)}
   | expression DIV expression {Div ($1, $3)}
;

comparaison:
     expression EGL expression {Egl ($1, $3)}
   | expression SUP expression {Sup ($1, $3)}
   | expression INF expression {Inf ($1, $3)}
   | expression SUPEGL expression {SupEgl ($1, $3)}
   | expression INFEGL expression {InfEgl ($1, $3)}
   | expression DIF expression {Dif ($1, $3)}
;

appel_expression:
     expression_simple {$1}
   | expression_simple appel_expression {Apl ($1, $2)}
;

expression_simple:
     IDF {Var $1}
   | UNIT {Unit}
   | INT {Int $1}
   | TRUE {Bool true}
   | FALSE {Bool false}
   | DEREF IDF {Deref (Var $2)}
   | PARG expression PARD {Par $2}
   | PARG expression VIRG expression PARD {Pair ($2, $4)}
;

liste_identifiants:
     identifiant liste_identifiants {$1::$2}
   | {[]}
;

identifiant:
     UNIT {Unit}
   | IDF {Var $1}
;
